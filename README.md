# DevOps Apprenticeship: Project Exercise

> If you are using GitPod for the project exercise (i.e. you cannot use your local machine) then you'll want to launch a VM using the [following link](https://gitpod.io/#https://github.com/CorndelWithSoftwire/DevOps-Course-Starter). Note this VM comes pre-setup with Python & Poetry pre-installed.

## System Requirements

The project uses poetry for Python to create an isolated environment and manage package dependencies. To prepare your system, ensure you have an official distribution of Python version 3.8+ and install Poetry using one of the following commands (as instructed by the [poetry documentation](https://python-poetry.org/docs/#system-requirements)):

### Poetry installation (Bash)

```bash
curl -sSL https://install.python-poetry.org | python3 -
```

### Poetry installation (PowerShell)

```powershell
(Invoke-WebRequest -Uri https://install.python-poetry.org -UseBasicParsing).Content | py -
```

## Dependencies

The project uses a virtual environment to isolate package dependencies. To create the virtual environment and install required packages, run the following from your preferred shell:

```bash
$ poetry install
```

You'll also need to clone a new `.env` file from the `.env.template` to store local configuration options. This is a one-time operation on first setup:

```bash
$ cp .env.template .env  # (first time only)
```

The `.env` file is used by flask to set environment variables when running `flask run`. This enables things like development mode (which also enables features like hot reloading when you make a file change). There's also a [SECRET_KEY](https://flask.palletsprojects.com/en/1.1.x/config/#SECRET_KEY) variable which is used to encrypt the flask session cookie.

## Running the App

Once the all dependencies have been installed, start the Flask app in development mode within the Poetry environment by running:
```bash
$ poetry run flask run
```

You should see output similar to the following:
```bash
 * Serving Flask app "app" (lazy loading)
 * Environment: development
 * Debug mode: on
 * Running on http://127.0.0.1:5000/ (Press CTRL+C to quit)
 * Restarting with fsevents reloader
 * Debugger is active!
 * Debugger PIN: 226-556-590
```
Now visit [`http://localhost:5000/`](http://localhost:5000/) in your web browser to view the app.

#Testing the App
Unit Tests written for app. Tests to be run using command
$ poetry run pytest

# Running/building app via Docker use the following commands in Prod/dev environments
# Note local .env file is passed in during Docker run.

# running in Production env 
docker build --target production --tag todo-app:prod .

#docker run --env-file ./.env -p 5001:5000 --mount "type=bind,source=$(pwd)/todo_app,target=/opt/#todo_app" todo-app:prod

docker run --env-file ./.env -p 8000:8000 --mount "type=bind,source=$(pwd)/todo_app,target=/opt/todo_app" todo-app:prod
# http://127.0.0.1:8000/ to run 
# http://localhost:8000/ to run app

# Running in Development env
docker build --target development --tag todo-app:dev .

docker run --env-file ./.env -p 5001:5000 --mount "type=bind,source=$(pwd)/todo_app,target=/opt/todo_app" todo-app:dev
# go to http://127.0.0.1:5000 to run app
# go to http://localhost:5001 to run app

# Docker Compose 
docker compose up --build dev
# go to http://127.0.0.1:5001 to run app

docker compose up --build prod
# go to http://127.0.0.1:8000/ to run app 

# Running in test env
# go to http://127.0.0.1:5000 to run app

Migrate To-to app to Azure Cloud platform
#Put Container Image on Docker Hub registry

#login to dockerhub
docker login

#Build Image and push to Docker 
docker build --platform=linux/amd64 --target production --tag conto1/todo-app:prod . --push

#Creating Azure App Service 
#Create a Resource -> Web App - TayoWebApp
#Select Project Exercise resource group - Cohort29_TayOlu_ProjectExercise
#In the “Publish” field, select “Docker Container”
#Create “App Service Plan” - TayoServicePlan
#On the next screen, select Docker Hub in the “Image Source” field, and enter the details of your image. - todo-app:prod

Following environment variables setup in settings -> configuration in portal
API_KEY
API_TOKEN
BOARD_ID
DOING_LISTID
DONE_LISTID
SECRET_KEY
TRELLO_BASE_URL
TRELLO_TODO_LIST_ID
WEBSITES_PORT

SITE is loaded to confirm it works by checking Acitivity log, Deployment center Logs, Log Stream  

WEBHOOK URL is located in Deployment centre 
it is tested by running cmd 
$ curl -dH -X POST "https://\$<deployment_username>:<deployment_password>@<webapp_name>.scm.azurewebsites.net/docker/hook"

link to website - https://tayowebapp.azurewebsites.net/

Module 9 - expand CI pipeline to publish build artefacts,
jobs added to test and deploy todo app to Docker and release to Azure
build and push your todo app to Docker Hub

# variables created on GITLAB

AZURE_WEBHOOK
CI_COMMIT_BRANCH
DOCKER_PASSWORD
DOCKER_USER
